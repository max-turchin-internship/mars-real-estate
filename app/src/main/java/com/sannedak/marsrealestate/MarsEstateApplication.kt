package com.sannedak.marsrealestate

import android.app.Application
import android.content.res.Resources
import com.sannedak.marsrealestate.data.network.RetrofitFactory
import com.sannedak.marsrealestate.data.repository.MarsEstateMockRepository
import com.sannedak.marsrealestate.data.repository.MarsEstateRetrofit
import com.sannedak.marsrealestate.data.repository.MarsRepositoryImpl
import com.sannedak.marsrealestate.presentation.detail.model.UiEstateDetailMapper
import com.sannedak.marsrealestate.presentation.overview.model.UiMarsEstateMapper

class MarsEstateApplication : Application() {

    companion object {
        lateinit var appResources: Resources
    }

    private val retrofit by lazy { RetrofitFactory.create() }
    private val nasaService by lazy { retrofit.create(MarsEstateRetrofit::class.java) }
    private val mockService by lazy { MarsEstateMockRepository() }

    private val _repository by lazy { MarsRepositoryImpl(mockService, nasaService) }
    private val _estateMapper by lazy { UiMarsEstateMapper() }
    private val _estateDetailMapper by lazy { UiEstateDetailMapper(appResources) }

    val repository
        get() = _repository

    val estateMapper
        get() = _estateMapper

    val estateDetailMapper
        get() = _estateDetailMapper

    override fun onCreate() {
        super.onCreate()
        appResources = resources
    }
}
