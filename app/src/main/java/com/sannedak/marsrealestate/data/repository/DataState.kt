package com.sannedak.marsrealestate.data.repository

import com.sannedak.marsrealestate.data.model.MarsEstateResponse

sealed class DataState<T> {

    object Downloading: DataState<Any>()
    object Error: DataState<Any>()
    data class Success<T>(val data: List<MarsEstateResponse>): DataState<T>()
}
