package com.sannedak.marsrealestate.data.repository

import com.sannedak.marsrealestate.data.model.ApiFilter
import kotlinx.coroutines.flow.flow

interface MarsEstateRepository {

    suspend fun getEstateList(filter: ApiFilter) = flow<DataState<Any>> { }
}