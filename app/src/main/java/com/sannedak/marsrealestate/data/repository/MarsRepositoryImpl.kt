package com.sannedak.marsrealestate.data.repository

import com.sannedak.marsrealestate.data.model.ApiFilter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MarsRepositoryImpl(
    private val mockService: MarsEstateMockRepository,
    private val nasaService: MarsEstateRetrofit
) : MarsEstateRepository {

    override suspend fun getEstateList(filter: ApiFilter) = getFromNet(filter)

    private fun getFromNet(filter: ApiFilter) = flow<DataState<Any>> {
        emit(DataState.Downloading)
        try {
            val response = nasaService.getEstateList(filter.string)
            emit(DataState.Success(response))
        } catch (e: Exception) {
            emit(DataState.Error)
        }
    }
}
