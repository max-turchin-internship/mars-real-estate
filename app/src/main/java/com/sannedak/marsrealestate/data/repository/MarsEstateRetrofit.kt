package com.sannedak.marsrealestate.data.repository

import com.sannedak.marsrealestate.data.model.MarsEstateResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MarsEstateRetrofit {

    @GET("realestate")
    suspend fun getEstateList(@Query("filter") type: String): List<MarsEstateResponse>
}
