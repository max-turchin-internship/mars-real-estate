package com.sannedak.marsrealestate.data.repository

import com.sannedak.marsrealestate.data.model.ApiFilter
import com.sannedak.marsrealestate.data.model.MarsEstateResponse
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow

class MarsEstateMockRepository {

    fun getEstateList(filter: ApiFilter) = flow {
        var list = emptyList<MarsEstateResponse>()
        emit(DataState.Downloading)
        delay(2000)
        if (filter == ApiFilter.SHOW_ALL)
            list = listOf(
                MarsEstateResponse(
                    424905,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000MR0044631300503690E01_DXXX.jpg",
                    450000.0,
                    "buy"
                ),
                MarsEstateResponse(
                    424906,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000ML0044631300305227E03_DXXX.jpg",
                    8000000.0,
                    "rent"
                ),
                MarsEstateResponse(
                    424907,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000MR0044631290503689E01_DXXX.jpg",
                    11000000.0,
                    "rent"
                ),
                MarsEstateResponse(
                    424908,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000ML0044631290305226E03_DXXX.jpg",
                    8000000.0,
                    "rent"
                )
            )
        if (filter == ApiFilter.SHOW_RENT)
            list = listOf(
                MarsEstateResponse(
                    424906,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000ML0044631300305227E03_DXXX.jpg",
                    8000000.0,
                    "rent"
                ),
                MarsEstateResponse(
                    424907,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000MR0044631290503689E01_DXXX.jpg",
                    11000000.0,
                    "rent"
                ),
                MarsEstateResponse(
                    424908,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000ML0044631290305226E03_DXXX.jpg",
                    8000000.0,
                    "rent"
                )
            )
        if (filter == ApiFilter.SHOW_BUY)
            list = listOf(
                MarsEstateResponse(
                    424905,
                    "http://mars.jpl.nasa.gov/msl-raw-images/msss/01000/mcam/1000MR0044631300503690E01_DXXX.jpg",
                    450000.0,
                    "buy"
                )
            )
        emit(DataState.Success(list))
    }
}
