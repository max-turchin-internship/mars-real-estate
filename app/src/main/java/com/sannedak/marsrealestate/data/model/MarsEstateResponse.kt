package com.sannedak.marsrealestate.data.model

import com.squareup.moshi.Json

data class MarsEstateResponse(

    val id: Int,
    @Json(name = "img_src")
    val imgSrc: String,
    val price: Double,
    val type: String
)

enum class ApiFilter(val string: String) {

    SHOW_RENT("rent"),
    SHOW_BUY("buy"),
    SHOW_ALL("all");
}
