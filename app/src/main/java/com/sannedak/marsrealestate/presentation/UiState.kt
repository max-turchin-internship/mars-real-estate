package com.sannedak.marsrealestate.presentation

import com.sannedak.marsrealestate.presentation.detail.EstateDetailParams
import com.sannedak.marsrealestate.presentation.detail.model.UiEstateDetail
import com.sannedak.marsrealestate.presentation.overview.model.UiMarsEstate

sealed class UiState<T> {

    object Loading : UiState<Any>()
    object Idle : UiState<Any>()
    object Error : UiState<Any>()
    data class TransitionToDetailScreen<T>(val params: EstateDetailParams) : UiState<T>()
    data class EstateOnScreen<T>(val data: List<UiMarsEstate>) : UiState<T>()
    data class EstateDetailsOnScreen<T>(val data: UiEstateDetail) : UiState<T>()
}
