package com.sannedak.marsrealestate.presentation.overview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sannedak.marsrealestate.R
import com.sannedak.marsrealestate.databinding.GridItemEstateOverviewBinding
import com.sannedak.marsrealestate.presentation.detail.EstateDetailParams
import com.sannedak.marsrealestate.presentation.overview.model.UiMarsEstate

class EstateOverviewGridAdapter(private val showDetails: (EstateDetailParams) -> Unit) :
    ListAdapter<UiMarsEstate, EstateGridItemViewHolder>(EstateGridDiffCallback()) {

    init {
        setHasStableIds(true)
    }

    private val list = mutableListOf<UiMarsEstate>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EstateGridItemViewHolder {
        return EstateGridItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.grid_item_estate_overview, parent, false
            )
        )
    }

    override fun getItemId(position: Int): Long {
        val positionInRecycler = list[position].id
        return positionInRecycler.toLong()

    }

    override fun onBindViewHolder(holder: EstateGridItemViewHolder, position: Int) {
        holder.bind(list[position], showDetails)
    }

    override fun getItemCount() = list.size

    fun refillList(newList: List<UiMarsEstate>) {
        list.clear()
        list.addAll(newList)
    }
}

class EstateGridItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = GridItemEstateOverviewBinding.bind(view)

    fun bind(listItem: UiMarsEstate, showDetails: (EstateDetailParams) -> Unit) {
        listItem.imgSrc.let {
            val imageUrl = it.toUri().buildUpon().scheme("https").build()
            Glide.with(binding.root)
                .load(imageUrl)
                .apply(RequestOptions())
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_outline_cloud_off_24)
                .into(binding.imageMarsEstate)
        }
        binding.imageMarsEstate.setOnClickListener {
            val params = EstateDetailParams(listItem.imgSrc, listItem.price, listItem.type)
            showDetails(params)
        }
    }
}

class EstateGridDiffCallback : DiffUtil.ItemCallback<UiMarsEstate>() {
    override fun areItemsTheSame(oldItem: UiMarsEstate, newItem: UiMarsEstate) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: UiMarsEstate, newItem: UiMarsEstate) =
        oldItem == newItem
}
