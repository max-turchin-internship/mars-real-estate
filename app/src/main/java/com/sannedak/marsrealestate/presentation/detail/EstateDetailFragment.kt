package com.sannedak.marsrealestate.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sannedak.marsrealestate.MarsEstateApplication
import com.sannedak.marsrealestate.R
import com.sannedak.marsrealestate.databinding.FragmentEstateDetailBinding
import com.sannedak.marsrealestate.presentation.UiState
import com.sannedak.marsrealestate.presentation.detail.model.UiEstateDetail
import com.sannedak.marsrealestate.presentation.setVisible
import kotlinx.coroutines.flow.collect

class EstateDetailFragment : Fragment() {

    private var _binding: FragmentEstateDetailBinding? = null

    private val args: EstateDetailFragmentArgs by navArgs()
    private val viewModel: EstateDetailViewModel by viewModels {
        EstateDetailViewModelFactory(MarsEstateApplication().estateDetailMapper, args)
    }

    private val binding
        get() = _binding!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEstateDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                when (it) {
                    is UiState.Loading -> showLoading()
                    is UiState.Error -> showError()
                    is UiState.EstateDetailsOnScreen -> showDetails(it.data)
                }
            }
        }
    }

    private fun showLoading() {
        binding.groupEstate.setVisible(false)
        binding.groupError.setVisible(false)
        binding.groupLoading.setVisible(true)
    }

    private fun showError() {
        binding.groupEstate.setVisible(false)
        binding.groupLoading.setVisible(false)
        binding.groupError.setVisible(true)
    }

    private fun showDetails(estate: UiEstateDetail) {
        binding.groupEstate.setVisible(true)
        binding.groupLoading.setVisible(false)
        binding.groupError.setVisible(false)
        binding.textEstateType.text = estate.type
        binding.textEstatePrice.text = estate.price
        Glide.with(this)
            .load(estate.imgSrc)
            .apply(RequestOptions())
            .placeholder(R.drawable.loading_animation)
            .error(R.drawable.ic_outline_cloud_off_24)
            .into(binding.imageEstate)
    }
}