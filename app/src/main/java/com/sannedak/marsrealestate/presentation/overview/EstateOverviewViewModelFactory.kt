package com.sannedak.marsrealestate.presentation.overview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sannedak.marsrealestate.data.repository.MarsEstateRepository
import com.sannedak.marsrealestate.presentation.overview.model.UiMarsEstateMapper

class EstateOverviewViewModelFactory(
    private val repository: MarsEstateRepository,
    private val mapper: UiMarsEstateMapper
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EstateOverviewViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return EstateOverviewViewModel(repository, mapper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
