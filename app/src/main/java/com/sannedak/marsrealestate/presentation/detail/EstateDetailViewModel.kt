package com.sannedak.marsrealestate.presentation.detail

import androidx.lifecycle.ViewModel
import com.sannedak.marsrealestate.presentation.UiState
import com.sannedak.marsrealestate.presentation.detail.model.UiEstateDetailMapper
import java.lang.Exception
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class EstateDetailViewModel(
    private val mapper: UiEstateDetailMapper,
    private val estateDetails: EstateDetailFragmentArgs
) : ViewModel() {

    private val _uiState = MutableStateFlow<UiState<Any>>(UiState.Idle)

    val uiState: StateFlow<UiState<Any>>
        get() = _uiState

    init {
        loadEstateDetails()
    }

    private fun loadEstateDetails() {
        _uiState.value = UiState.Loading
        try {
            val mappedEstate = mapper.map(estateDetails)
            _uiState.value = UiState.EstateDetailsOnScreen(mappedEstate)
        } catch (e: Exception) {
            _uiState.value = UiState.Error
        }

    }
}