package com.sannedak.marsrealestate.presentation.detail.model

import android.content.res.Resources
import com.sannedak.marsrealestate.R
import com.sannedak.marsrealestate.presentation.detail.EstateDetailFragmentArgs
import com.sannedak.marsrealestate.presentation.overview.model.EstateType

class UiEstateDetailMapper(private val resources: Resources) {

    fun map(estate: EstateDetailFragmentArgs) = UiEstateDetail(
        estate.Estate.imgSrc,
        mapEstatePrice(estate.Estate.price, estate.Estate.type),
        mapEstateType(estate.Estate.type)
    )

    private fun mapEstatePrice(estatePrice: Double, estateType: EstateType): String {
        return if (estateType == EstateType.BUY) resources.getString(
            R.string.display_price,
            estatePrice
        )
        else resources.getString(R.string.display_price_monthly_rental, estatePrice)
    }

    private fun mapEstateType(estateType: EstateType): String {
        return if (estateType == EstateType.BUY) resources.getString(R.string.type_sale)
        else resources.getString(R.string.type_rent)
    }
}