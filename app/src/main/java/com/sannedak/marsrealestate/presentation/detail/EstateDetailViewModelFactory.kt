package com.sannedak.marsrealestate.presentation.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sannedak.marsrealestate.presentation.detail.model.UiEstateDetailMapper

class EstateDetailViewModelFactory(
    private val mapper: UiEstateDetailMapper,
    private val estateDetails: EstateDetailFragmentArgs
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EstateDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return EstateDetailViewModel(mapper, estateDetails) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
