package com.sannedak.marsrealestate.presentation.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.sannedak.marsrealestate.MarsEstateApplication
import com.sannedak.marsrealestate.R
import com.sannedak.marsrealestate.data.model.ApiFilter
import com.sannedak.marsrealestate.databinding.FragmentEstateOverviewBinding
import com.sannedak.marsrealestate.presentation.UiState
import com.sannedak.marsrealestate.presentation.detail.EstateDetailParams
import com.sannedak.marsrealestate.presentation.overview.model.UiMarsEstate
import com.sannedak.marsrealestate.presentation.setVisible
import kotlinx.coroutines.flow.collect


class EstateOverviewFragment : Fragment() {

    private var _binding: FragmentEstateOverviewBinding? = null

    private val gridAdapter by lazy { EstateOverviewGridAdapter() }
    private val application by lazy { (requireActivity().application as MarsEstateApplication) }
    private val viewModel: EstateOverviewViewModel by viewModels {
        EstateOverviewViewModelFactory(application.repository, application.estateMapper)
    }

    private val binding
        get() = _binding!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEstateOverviewBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecycler()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.overflow_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.updateFilter(
            when (item.itemId) {
                R.id.show_rent_menu -> ApiFilter.SHOW_RENT
                R.id.show_buy_menu -> ApiFilter.SHOW_BUY
                else -> ApiFilter.SHOW_ALL
            }
        )
        return true
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                when (it) {
                    is UiState.Loading -> showLoading()
                    is UiState.Error -> showError()
                    is UiState.EstateOnScreen -> showEstate(it.data)
                    is UiState.TransitionToDetailScreen -> showDetailsScreen(it.params)
                }
            }
        }
    }

    private fun setUpRecycler() {
        val manager = GridLayoutManager(activity, 2)
        binding.gridMarsEstate.layoutManager = manager
        binding.gridMarsEstate.adapter = gridAdapter
        binding.gridMarsEstate.itemAnimator = null // Workaround for gridLayout issue
    }

    private fun showLoading() {
        binding.gridMarsEstate.setVisible(false)
        binding.groupError.setVisible(false)
        binding.groupLoading.setVisible(true)
    }

    private fun showError() {
        binding.gridMarsEstate.setVisible(false)
        binding.groupLoading.setVisible(false)
        binding.groupError.setVisible(true)
    }

    private fun showEstate(data: List<UiMarsEstate>) {
        binding.gridMarsEstate.setVisible(true)
        binding.groupLoading.setVisible(false)
        binding.groupError.setVisible(false)
        gridAdapter.refillList(data)
        gridAdapter.submitList(data)
    }

    private fun showDetailsScreen(estate: EstateDetailParams) {
        val action = EstateOverviewFragmentDirections
            .actionEstateOverviewFragmentToEstateDetailFragment(estate)
        this.findNavController().navigate(action)
    }
}
