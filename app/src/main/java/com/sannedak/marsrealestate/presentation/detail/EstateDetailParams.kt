package com.sannedak.marsrealestate.presentation.detail

import android.os.Parcelable
import com.sannedak.marsrealestate.presentation.overview.model.EstateType
import kotlinx.parcelize.Parcelize

@Parcelize
class EstateDetailParams(

    val imgSrc: String,
    val price: Double,
    val type: EstateType
) : Parcelable
