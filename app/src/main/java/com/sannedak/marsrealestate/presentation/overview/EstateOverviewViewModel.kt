package com.sannedak.marsrealestate.presentation.overview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sannedak.marsrealestate.data.model.ApiFilter
import com.sannedak.marsrealestate.data.model.MarsEstateResponse
import com.sannedak.marsrealestate.data.repository.DataState
import com.sannedak.marsrealestate.data.repository.MarsEstateRepository
import com.sannedak.marsrealestate.presentation.UiState
import com.sannedak.marsrealestate.presentation.overview.model.UiMarsEstateMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class EstateOverviewViewModel(
    private val repository: MarsEstateRepository,
    private val mapper: UiMarsEstateMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<UiState<Any>>(UiState.Idle)

    val uiState: StateFlow<UiState<Any>>
        get() = _uiState

    init {
        loadEstate(ApiFilter.SHOW_ALL)
    }

    fun updateFilter(filter: ApiFilter) {
        loadEstate(filter)
    }

    private fun loadEstate(filter: ApiFilter) {
        viewModelScope.launch {
            val dataFlow = repository.getEstateList(filter)
            dataFlow.collect {
                when (it) {
                    is DataState.Downloading -> _uiState.value = UiState.Loading
                    is DataState.Error -> _uiState.value = UiState.Error
                    is DataState.Success -> mapToUiObject(it.data)
                }
            }
        }
    }

    private fun mapToUiObject(list: List<MarsEstateResponse>) {
        val mappedList = mapper.map(list)
        _uiState.value = UiState.EstateOnScreen(mappedList)
    }
}
