package com.sannedak.marsrealestate.presentation.detail.model

data class UiEstateDetail(

    val imgSrc: String,
    val price: String,
    val type: String
)
