package com.sannedak.marsrealestate.presentation.overview.model

data class UiMarsEstate(

    val id: Int,
    val imgSrc: String,
    val price: Double,
    val type: EstateType
)

enum class EstateType {

    BUY,
    RENT
}