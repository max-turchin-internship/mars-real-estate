package com.sannedak.marsrealestate.presentation.overview.model

import com.sannedak.marsrealestate.data.model.MarsEstateResponse

class UiMarsEstateMapper {

    fun map(list: List<MarsEstateResponse>) =
        list.map { UiMarsEstate(it.id, it.imgSrc, it.price, mapEstateType(it.type)) }

    private fun mapEstateType(type: String): EstateType {
        return if (type == "buy") EstateType.BUY
        else EstateType.RENT
    }
}