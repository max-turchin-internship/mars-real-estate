object BuildOptions {

    object Versions {
        const val buildToolsVersion = "4.1.1"
        const val kotlinVersion =  "1.4.21"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.buildToolsVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinParcelize = "kotlin-parcelize"
    const val navigationSafeArgs = "androidx.navigation.safeargs.kotlin"
}

object AndroidSdk {

    const val minimal = 27
    const val compile = 30
    const val target = compile
}

object Config {

    const val appId = "com.sannedak.marsrealestate"
    const val code = 1
    const val name = "1.0"

    const val testRunner = "androidx.test.runner.AndroidJUnitRunner"
}
